// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
	apiKey: "AIzaSyBi3cWKKyKJEMIbqBYE8YSEalYTx4zyrEw",
    authDomain: "table-designed.firebaseapp.com",
    databaseURL: "https://table-designed.firebaseio.com",
    projectId: "table-designed",
    storageBucket: "table-designed.appspot.com",
    messagingSenderId: "299962291842"
  }
};
