import { Component } from '@angular/core';

@Component({
  selector: 'merchants-table',
  template: '../../../views/merchants/merchants-table/merchants-table.component.html',
  styleUrls: ['../../../views/merchants/merchants-table/merchants-table.component.css']
})

export class MerchantsTableComponent{
  selectedBiller = "Authorize.net"
}
