import { Component } from '@angular/core';
import { MerchantAuthentication, TransactionRequest, CreditCard } from '../../models/merchants/merchants';
import { CustomerCardsComponent } from '../customer/customer-cards/customer-cards.component';
import { MerchantsService } from '../../services/merchants/merchants.service';
import { AuthServ } from '../../services/auth/auth-service.service';


@Component({
  selector: 'merchants',
  templateUrl: '../../views/merchants/merchants.component.html',
  styleUrls: ['../../views/merchants/merchants.component.css'],
  providers: [MerchantsService, AuthServ]
})

export class MerchantsComponent{
  biller = "authorize";
  auth =  new MerchantAuthentication();
  transaction = new TransactionRequest();
  creditCard = new CreditCard();



  constructor(private mrcSvc: MerchantsService){
  }

  saveAuth(object){
    this.mrcSvc.saveMerchant(object, this.biller)
  }

}
