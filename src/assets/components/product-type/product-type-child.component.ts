import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import { Products } from '../../models/products/product-cards/product-cards';
import { Venue } from '../../models/products/venue';
import { ProductFormService } from '../../services/products/product-form/product-form.service'

@Component({
  selector: 'product-type-child',
  templateUrl: '../../views/products/product-type/product-type-child.component.html',
  styleUrls: ['../../views/products/product-type/product-type-child.component.css'],
  providers: [ ProductFormService ]
})

export class ProductTypeChildComponent implements OnChanges{

product: Products;
venue: Venue;

@Input('productType') productType: string;

selectedProductType: any;

    constructor(private formSvc: ProductFormService){
      this.product = new Products();
      this.venue = new Venue();
    }


    addProduct(products: Products){
      this.formSvc.addProduct(products)
    }

    ngOnChanges(changes: SimpleChanges){
      console.log(this.productType);
    }



}
