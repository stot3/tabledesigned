import { Component, Output, Input, OnChanges, SimpleChanges, EventEmitter, ElementRef } from '@angular/core';
import { ProductTypeChildComponent } from '../product-type/product-type-child.component';
import { Venue } from '../../models/products/venue';
import { Products } from '../../models/products/product-cards/product-cards';
@Component({
  selector: 'product-selector',
  templateUrl: '../../views/products/product-type/product-type.component.html',
  styleUrls: ['../../views/products/product-type/product-type.component.css']
})


export class ProductSelectorComponent{
  productType = [
  {value: "generic", name: "Generic"},
  {value: "venue", name: "Venue"},
  {value: "catering", name: "Catering" },
  {value: "production", name: "Production"},
  {value: "entertainment", name: "Entertainment"},
  {value: "transportation", name: "Transportation"},
  {value: "lodging", name: "Lodging"},
  {value: "conference", name: "Conference"}
];
@Output() selectedProduct = new EventEmitter<string>();


clickedProd(stream: string){
  this.selectedProduct.emit(stream);
}

}
