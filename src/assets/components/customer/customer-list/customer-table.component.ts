import {Component} from '@angular/core'
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {AngularFireDatabase} from 'angularfire2/database';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'customer-table',
  templateUrl: 'customer-table.html',
  styleUrls: ['customer-table.css']
})

export class CustomerData
{
  firstName: string;
  lastName: string;
}

export class CustomerTableComponent
{
  displayedColumns = ['First Name', 'Last Name'];
}

