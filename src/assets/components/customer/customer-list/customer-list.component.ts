import {Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren, Input} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import { FirebaseTableService, FirebaseTableDataSource } from '../../../services/customer/customer-list/customer-list.service'
import { Renderer2 } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'customer-list',
  styleUrls: ['../../../views/customer/customer-list/customer-list.component.css'],
  templateUrl: '../../../views/customer/customer-list/customer-list.component.html'
})

export class CustomerListComponent implements OnInit {
   displayedColumns = ['firstName', 'lastName'];
   dataSource: FirebaseTableDataSource | null;
   firebaseTabSvc = new FirebaseTableService(this.database);
   eventEmitter: EventEmitter<any> = new EventEmitter();



   @ViewChild('filter') filter: ElementRef;
   rowView: ElementRef;


  constructor(private database: AngularFireDatabase) {
    this.dataSource = new FirebaseTableDataSource(this.firebaseTabSvc);

  }

  selectedUser(row) {
    if(row.selectedUser === true)
      {row.selectedUser = false;}
    else
    {row.selectedUser = true;}
    console.log(row); 
    }

  tableChanges = (index: string, item: any) => index;




   ngOnInit() {
     Observable.fromEvent(this.filter.nativeElement, 'keyup')
       .debounceTime(150)
       .distinctUntilChanged()
       .subscribe(() => {
           if (!this.dataSource) {
             return;
           }
           this.dataSource.filter = this.filter.nativeElement.value
         }

       )
   }

}
