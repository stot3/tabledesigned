import { Component } from '@angular/core';
import { CustomerCardsComponent} from '../../customer/customer-cards/customer-cards.component';
import { ProductTableComponent } from '../../product-upload/product-table/product-table.component';
import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from 'rxjs/Observable';
import * as firebase from 'firebase/app';



export class Customer{
  constructor(
    public company: string,
    public firstName: string,
    public lastName: string,
    public middleI: string,
    public address: string,
    public address2: string,
    public city: string,
    public state: string,
    public email: string
  ){}
}




@Component({
  selector: 'customer-entry',
  templateUrl: '../../../views/customer/customer-entry/customer-entry.component.html',
  styleUrls: ['../../../views/customer/customer-entry/customer-entry.component.css'],


})

export class CustomerEntryComponent {
  public userCustomer =  new Customer('', '', '', '', '', '', '', '', '');
  userList: FirebaseListObservable<any[]>;
  userIndex: FirebaseObjectObservable<any>;
  uid: string;
  email: string;
  billSameShip = "true";
  basePath = '/app/users/';
  constructor(private db: AngularFireDatabase, private signIn: AngularFireAuth)
  {
    this.signIn.auth.onAuthStateChanged((user) => {

      if (this.uid !== null) {
        this.basePath += user.uid;
        this.email = user.email;
        this.userList = db.list(`${this.basePath}`);
      }
      else {
        if (this.signIn.auth.currentUser.uid === null) {
          console.log('No auth information.');
        }
      }
    },
      (error) => {
      console.log(error);
      })

  }

  save(customer: Customer){
    if (this.uid !== null) {

      this.userList = this.db.list('app/users/customerList')

      let shipAddressInfo = {}

      if (this.billSameShip === "true")
      {
        shipAddressInfo =
          {
            shipAddress: customer.address,
            shipAddress2: customer.address2,
            shipState: customer.state,
            shipCity: customer.city,
            shipFirstName: customer.firstName,
            shipLastName: customer.lastName
          }
        Object.assign(customer, shipAddressInfo)

      }
      if (this.userCustomer)

      this.userList.push(customer).then(
        () => {



          this.userList = this.db.list(`app/users/customerNames`)
          this.userList.push({
            firstName : customer.firstName,
            lastName : customer.lastName
          })


          console.log(this.basePath);
        },
        (error) => {console.log(error)});
    }



  }
}
