import { Component } from '@angular/core';
import { EventEmitter } from '@angular/core'
import { Observable } from 'rxjs/Observable';
import {AuthServ} from '../../services/auth/auth-service.service';
import { SignUpService } from '../../services/sign-up/sign-up.service';

@Component({
	selector: 'signin',
	styleUrls: ['../../views/auth/auth.component.css'],
	templateUrl: '../../views/auth/auth.component.html',
	providers: [AuthServ, SignUpService]

})

export class SigninComponent {

	user: Observable<firebase.User>;

	constructor(private auSrv: AuthServ, private signup: SignUpService ){
		this.user = auSrv.retUid();
		console.log(this.auSrv.isSignedIn)
	}

	register(){}

	login = this.auSrv.login
	logout = this.auSrv.logout




}
