import { Component } from '@angular/core';
import { CustomerListComponent} from '../../customer/customer-list/customer-list.component';
import { UploadService } from '../../../services/products/product-upload/product-upload.service';
import { AuthServ } from '../../../services/auth/auth-service.service';
import {OnInit} from '@angular/core';

@Component({
  selector: 'package-creator',
  templateUrl: '../../../views/orders/package-creator/package-creator.component.html',
  styleUrls: ['../../../views/orders/package-creator/package-creator.component.css'],
  providers: [UploadService, AuthServ]


})

export class PackageCreatorComponent implements OnInit{

  imageArray: Array<any>

  constructor(private upSvc: UploadService){}
  ngOnInit() {

  this.imageArray = this.upSvc.getImagesUrl();
  console.log(this.upSvc.getImages(this.imageArray[0].url))

  }


}
