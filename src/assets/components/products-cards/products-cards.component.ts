import { Component } from '@angular/core';


@Component({
  selector: 'products-cards',
  templateUrl: '../../views/products/products-cards/products-cards.component.html',
  styleUrls: ['../../views/products/products-cards/products-cards.component.css']
})

export class ProductsCardsComponent {
  columns: 4
}
