import { Component } from '@angular/core';
import { ProductTableDatabaseWorker, ProductTableDataSource} from '../../../services/products/product-table/product-table.service'
import { OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
@Component({
  selector: 'product-table',
  templateUrl: '../../../views/products/product-table/product-table.component.html',
  styleUrls: ['../../../views/products/product-table/product-table.component.css'],
})

export class ProductTableComponent implements OnInit{

  displayedColumns = ['name','description','price','qty'];
  tableDatabase = new ProductTableDatabaseWorker(this.database);
  dataSource: ProductTableDataSource | null;

  constructor(private database: AngularFireDatabase) {

  }

  ngOnInit(){
    this.dataSource = new ProductTableDataSource(this.tableDatabase);
  }

}
