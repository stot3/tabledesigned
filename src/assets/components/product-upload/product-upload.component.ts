import { Component, Input } from '@angular/core';
import { MdCardModule, MdButtonModule, MdToolbarModule } from '@angular/material';
import { UploadService } from '../../services/products/product-upload/product-upload.service';
import { AuthServ } from '../../services/auth/auth-service.service';
import { Upload } from '../../models/products/product-upload/upload';
import * as _ from 'lodash';
import { ProductTableComponent } from '../product-upload/product-table/product-table.component'
import { ProductSelectorComponent } from '../product-type/product-type.component';


@Component({
	selector: 'product-upload',
	templateUrl: '../../views/products/product-Upload/product-upload.component.html',
	styleUrls: ['../../views/products/product-Upload/product-upload.component.css'],
  providers: [UploadService, AuthServ]

})

export class ProductUploaderComponent {

tableFiles: FileList;
chairFiles: FileList;
centerpieceFiles: FileList;
flowerFiles: FileList;
currentUpload: Upload;
selectedProductType: string;





	constructor(private upSvc: UploadService){}

	selectedProduct(productType: string){
		this.selectedProductType = productType;
		console.log(this.selectedProductType); 
	}

  save(event){
    this.tableFiles = event.target.files;
  }

  uploadSingle(){
    const file = this.tableFiles.item(0);
    this.currentUpload = new Upload(file);
    this.upSvc.pushUpload(this.currentUpload);
  }

  uploadMultiple(){
    const files = this.tableFiles;
    const filesIndex =  _.range(files.length);
    _.each(filesIndex, (idx) => {
      this.currentUpload = new Upload(files[idx]);
      this.upSvc.pushUpload(this.currentUpload);
    })
  }
}
