import { Component, Input } from '@angular/core';
import {Products} from '../../../models/products/product-cards/product-cards';
import { ProductFormService } from '../../../services/products/product-form/product-form.service'

@Component({
    selector: 'product-form',
    templateUrl: '../../../views/products/product-form/product-form.component.html',
    styleUrls: ['../../../views/products/product-form/product-form.component.css'],
    providers: [ ProductFormService ]
  })



export class ProductFormComponent{
@Input() product: Products
  constructor(private formSvc: ProductFormService){
    this.product = new Products();
  }


  addProduct(products: Products){
    this.formSvc.addProduct(products)
  }

}
