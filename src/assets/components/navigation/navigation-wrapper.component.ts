import { Component } from '@angular/core';
import { MdMenuModule } from '@angular/material';
import { AuthServ } from '../../services/auth/auth-service.service';



@Component({
	selector: 'navigation',
	templateUrl: '../../views/navigation/navigation-wrapper.component.html',
	styleUrls: ['../../views/navigation/navigation-wrapper.component.css'],
	providers: [AuthServ]
})

export class NavigationWrapper {
	home = true;
	constructor(private auSrv: AuthServ){
		this.auSrv.retUid().debounceTime(300).subscribe((user) => {user? this.home = true: console.log("not ready")}).unsubscribe();
	}

}
