import {Component, Input} from '@angular/core';
import {MdSnackBar} from '@angular/material';
import {Observable} from 'rxjs/observable';

@Component({
  selector: 'snack-bar',
  template: '',
  styleUrls: ['../../views/snack-bar/snack-bar.component.css']
})

export class SnackBarComponent {
  @Input('message') message: string;
  @Input('component') component: Component;

  constructor(private snackBar: MdSnackBar) {
  }

  open(message) {
    const snack = this.snackBar.open(message, 'error')
  }




}
