export class MerchantAuthentication{
  name: string;
  transactionKey: string;
}
export class TransactionRequest{
  transactionType: string;
  amount: number;
}
export class CreditCard{
  cardNumber: number;
  expirationDate: Date;
  cardCode: number;
}

export class Profile{
  createProfile = false;
}

export class Merchant{
  MerchantAuthentication: MerchantAuthentication;
  refId: string;
  transactionRequest: TransactionRequest;
}
