export class Company {
  companyUid: string;
  companyName: string;
  companyEmail: string;
  companyPhone: string;
  companyAddress: string;
  companyCity: string;
  companyZip: number;
  companyState: string;
}
