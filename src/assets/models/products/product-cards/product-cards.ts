export class Products{
  name: string;
  description: string;
  price: number;
  qty: number;
  photoUrl?: string;
}

export interface ProductsInterface{
  name: string;
  description: string;
  price: number;
  qty: number;
  photoUrl?: string;
}
