import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {MerchantAuthentication} from '../../models/merchants/merchants';
import {AuthServ} from '../auth/auth-service.service';
import {Observable} from 'rxjs/observable';

@Injectable()

export class MerchantsService{

  uid: string;
  basePath = 'app/company/merchants';

  constructor(private db: AngularFireDatabase, private auServ: AuthServ){}

  saveMerchant = (object, billerType) => {

    const promise = new Promise(
      (resolve, reject) => {
        const user = this.auServ.retUid().subscribe((user)=>{resolve(user.uid)});
      }
    ).then(
      (uid) => {
        console.log(uid)
        this.basePath = `app/company/${uid}/merchants/billerType/${billerType}`;
        const dbRef = this.db.object(`${this.basePath}`);
        dbRef.set(object).then( () =>{},
                                err => err
                              )
      },
      (err) => {
        console.log(err)
      }
    )

  }

  grabMerchant = ()=>{}
}
