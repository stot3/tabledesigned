import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/observable';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {SnackBarComponent} from '../../components/snack-bar/snack-bar.component'
import {MdSnackBar} from '@angular/material';


@Injectable()

export class AuthServ
{

    user: Observable<firebase.User>;
    isSignedIn = false;
    snack: MdSnackBar; 

    constructor(private afAuth: AngularFireAuth)
    {
      this.user = afAuth.authState;
      this.authStateListener();
    }

    authStateListener(){
      this.afAuth.auth.onAuthStateChanged(
        (user) => {if (user){this.isSignedIn = true}},
        (err) => { const snackBar = new SnackBarComponent(this.snack).open(err);
        }
      )
    }

  	login = () => {
  	  this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then( user => user,
      (err) => {
        const snackBar = new SnackBarComponent(this.snack).open(err)
       } );
  	};


  	logout = () => {
  	this.afAuth.auth.signOut();
  	}

    retUid = () => this.user;




}
