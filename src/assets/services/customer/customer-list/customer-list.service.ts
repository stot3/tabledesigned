import {DataSource} from '@angular/cdk/table';
import {Observable} from 'rxjs/Observable';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import {fromPromise} from 'rxjs/observable/fromPromise';
import {Injectable} from '@angular/core';
import {CustomerListComponent} from '../../../components/customer/customer-list/customer-list.component';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ElementRef} from '@angular/core';


@Injectable()



export class CustomerData {
  firstName: string;
  lastName: string;
  key: string;
}

export class RowViewChange{
backgroundColor: any
}

export class FirebaseTableService {
  databaseInfo: FirebaseListObservable<any>;
  dataChange: BehaviorSubject<CustomerData[]> = new BehaviorSubject<CustomerData[]>([]);
  viewChange: BehaviorSubject<RowViewChange[]> = new BehaviorSubject<RowViewChange[]>([]);

  get data(): CustomerData[] {return this.dataChange.value}
  constructor(private db: AngularFireDatabase) {

    this.tableConfig().customerNames()
  }

  tableConfig = () => {
    this.databaseInfo = this.db.list('app/users/customerNames', {preserveSnapshot: true});
    const data = [];
    return {
      headers: () => this.databaseInfo,
      customerNames: () => {this.databaseInfo.subscribe(rows => rows.forEach( (row) => {
        data.push(
          {
            firstName: row.val().firstName,
            lastName: row.val().lastName,
            key: row.key,
            selectedUser: false
          }
        );
        this.dataChange.next(data);
        this.viewChange.next(data.filter((row) => console.log(row.backgroundColor)));
      } ))}


    };
  }

  updateTable = (key, item) => {
    this.databaseInfo.update(key, item)
  }
}

export class FirebaseTableDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('')
  userChange: BehaviorSubject<CustomerData> = new BehaviorSubject<CustomerData>({
    firstName: '',
    lastName: '',
    key: ''
  })
  rowChange = new BehaviorSubject<RowViewChange>({
    backgroundColor: ''
  });

  get filter(): string {return this.filterChange.value}
  set filter(filter: string) {this.filterChange.next(filter)}

  get row(): RowViewChange {return this.rowChange.value}
  set row(row: RowViewChange) {this.rowChange.next(row)}

  constructor(private fireSvc: FirebaseTableService) { super(); }

  connect(): Observable<CustomerData[]> {
    const displayTableChanges = [
      this.fireSvc.viewChange,
      this.fireSvc.dataChange,
      this.filterChange
    ]

    return Observable.merge(...displayTableChanges).map(
      () => this.fireSvc.data.slice().filter(
        (item: CustomerData, rowChange) => {
          const strSearch = item.firstName.toLowerCase();
            return strSearch.indexOf(this.filter.toLowerCase()) !== -1;
        }
      )
    )



  }

  disconnect() {}



}
