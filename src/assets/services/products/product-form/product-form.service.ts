import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {Products} from '../../../../assets/models/products/product-cards/product-cards';
import { ProductTableDatabaseWorker } from '../../products/product-table/product-table.service';


@Injectable()


export class ProductFormService {

productTableWorker: ProductTableDatabaseWorker;
constructor(private db: AngularFireDatabase){}

  addProduct(product: Products){
    const basePath = "app/products";
    const dbRef = this.db.list(`${basePath}`);
    dbRef.push(product).then(()=>{console.log("success")},
                             (err) => {console.log("error")});
    this.productTableWorker.initStream;
  }
}
