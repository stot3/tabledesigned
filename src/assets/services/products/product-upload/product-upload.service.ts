import { Injectable } from '@angular/core';
import { Upload } from '../../../models/products/product-upload/upload';
import * as firebase from 'firebase/storage';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from 'rxjs/observable';
import {AuthServ} from '../../auth/auth-service.service'

@Injectable()

export class UploadService{
  private user: Observable<firebase.User>;
  private basePath = `/item/users/`;
  private uploadTask: firebase.storage.UploadTask;

  pushUpload(upload: Upload){

    const storageRef = firebase.storage().ref();
    this.uploadTask = storageRef.child(`${this.basePath}/images/tables/${upload.file.name}`).put(upload.file);

    this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
      upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100

    },
      (error) => {
      console.log(error);
    },
      () => {
        upload.url = this.uploadTask.snapshot.downloadURL;
        upload.name = upload.file.name;
        this.saveFileData(upload);
      })

  }

  deleteUpload(upload: Upload){
    this.deleteFileData(upload.$key).then(
      () => {
        this.deleteFileStorage(upload.name)
      }

    ).catch(error => console.log(error))
  }
  private saveFileData(upload: Upload){
    this.db.list(`${this.basePath}/images/tables`).push(upload)
  }
  private deleteFileData(key: string){
    return new Promise(function(resolve, reject){
      return resolve(this.db.list(`${this.basePath}/`).remove(key));
    })
  }

  private deleteFileStorage(name: string){
    const storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/images/tables/${name}`).delete()


  }

  getImagesUrl(){

      const urls = []
      const dbRef = this.db.list(`${this.basePath}/images/tables`, {preserveSnapshot: true});
      dbRef.subscribe(snapshots => snapshots.forEach( (snapshot) => urls.push(snapshot.val() ) ) );
      return urls
    }

  getImages = (url) => new Promise(

    (resolve, reject) =>
  {
      let image = new Image();

      image.onload = () => {
        resolve(image)
      }

      image.onerror = () => {
        let message = "couldn't load at " + url
        reject(new Error(message))
      }
      image.src = url
  }
)

  constructor(private db: AngularFireDatabase, afSvc: AuthServ){
    this.user = afSvc.retUid()
    this.user.subscribe(
      (user) => {this.basePath += user.uid}
    )
  }

}
