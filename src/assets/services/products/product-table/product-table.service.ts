import { Injectable, OnInit } from '@angular/core';
import { ProductsInterface } from '../../../models/products/product-cards/product-cards';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/observable'
import { DataSource } from '@angular/cdk/table';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database';

@Injectable()

export class ProductTableDatabaseWorker implements OnInit{
  productsStream: BehaviorSubject<ProductsInterface[]> = new BehaviorSubject<ProductsInterface[]>([]);

  get stream(): ProductsInterface[] {return this.productsStream.value}

  constructor(private db: AngularFireDatabase){
          this.initStream()
   }

  initStream() {
    const copiedProducts = this.stream.slice()
    const basePath = 'app/products'
    const productList = this.db.list(`${basePath}`, {preserveSnapshot: true})
    productList.subscribe(products => products.forEach( (product) => {

      copiedProducts
      .push(
      {
        name: product.val().name,
        description: product.val().description,
        price: product.val().price,
        qty: product.val().qty
      }
    )

    this.productsStream.next(copiedProducts)
  }))

}

  ngOnInit()
  {

  }



}

export class ProductTableDataSource extends DataSource<any>{

  constructor(private productTableDatabase: ProductTableDatabaseWorker){
    super();
  }

  connect(): Observable<ProductsInterface[]>{
    return this.productTableDatabase.productsStream
  }

  disconnect() {}
}
