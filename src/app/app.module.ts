import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavigationWrapper } from '../assets/components/navigation/navigation-wrapper.component';
import { DashboardComponent } from '../assets/components/dashboard/dashboard.component';
import { ProductUploaderComponent } from '../assets/components/product-upload/product-upload.component';
import { CustomerEntryComponent } from '../assets/components/customer/customer-entry/customer-entry.component';
import { SigninComponent } from '../assets/components/auth/auth.component';
import { PackageCreatorComponent } from '../assets/components/orders/package-creator/package-creator.component';
import { ProductTableComponent } from '../assets/components/product-upload/product-table/product-table.component'
import { ProductsCardsComponent } from '../assets/components/products-cards/products-cards.component';
import {CustomerListComponent} from '../assets/components/customer/customer-list/customer-list.component';
import { CustomerCardsComponent } from '../assets/components/customer/customer-cards/customer-cards.component';
import { MerchantsComponent } from '../assets/components/merchants/merchants.component';
import { ReportsComponent } from '../assets/components/reports/reports.component';
import { ProductSelectorComponent} from '../assets/components/product-type/product-type.component';
import { ProductTypeChildComponent } from '../assets/components/product-type/product-type-child.component';
import { RegisterComponent } from '../assets/components/register/register.component'

import { SnackBarComponent } from '../assets/components/snack-bar/snack-bar.component';
import {FirebaseTableDataSource, FirebaseTableService} from '../assets/services/customer/customer-list/customer-list.service';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { CdkTableModule } from '@angular/cdk/table';

import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MD_PLACEHOLDER_GLOBAL_OPTIONS} from '@angular/material';
import { MdButtonModule, MdSidenavModule, MdCardModule, MdInputModule, MdToolbarModule, MdGridListModule, MdTableModule, MdSnackBarModule, MdCheckboxModule, MdSelectModule } from '@angular/material';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
  declarations: [
    AppComponent,
	  NavigationWrapper,
	  DashboardComponent,
	  ProductUploaderComponent,
	  SigninComponent,
	  CustomerEntryComponent,
    PackageCreatorComponent,
    ProductsCardsComponent,
    ProductTableComponent,
    CustomerListComponent,
    CustomerCardsComponent,
    MerchantsComponent,
    ReportsComponent,
    SnackBarComponent,
    ProductSelectorComponent,
    ProductTypeChildComponent,
    RegisterComponent



  ],
  imports: [
    BrowserModule,
    FormsModule,
	  AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
	  RouterModule.forRoot([
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'product-uploader',
        component: ProductUploaderComponent
      },
      {
        path: 'signin',
        component: SigninComponent
      },
      {
        path: 'customer-entry',
        component: CustomerEntryComponent
      },
      {
        path: 'customer-list',
        component: CustomerListComponent
      },
      {
        path: 'package-creator',
        component: PackageCreatorComponent
      },
      {
        path: 'merchants',
        component: MerchantsComponent
      },
      {
        path: 'reports',
        component: ReportsComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: '',
        redirectTo: '/signin',
        pathMatch: 'full'

      }
]),
	  BrowserAnimationsModule,

	  MdButtonModule,
	  MdSidenavModule,
	  MdCardModule,
	  MdInputModule,
    MdToolbarModule,
    MdTableModule,
    MdGridListModule,
    CdkTableModule,
    MdSnackBarModule,
    MdCheckboxModule,
    InfiniteScrollModule,
    MdSelectModule,

  ],
  providers: [
    {provide: MD_PLACEHOLDER_GLOBAL_OPTIONS, useValue: { float: 'always' }}
  ],
  bootstrap: [AppComponent],
})




export class AppModule {


}
