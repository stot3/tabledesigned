import { TableDesignedPage } from './app.po';

describe('table-designed App', () => {
  let page: TableDesignedPage;

  beforeEach(() => {
    page = new TableDesignedPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
